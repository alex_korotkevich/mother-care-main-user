import React, { useState } from "react";
import { BrowserRouter } from "react-router-dom";
import { Auth } from "./auth/auth";
import { Home } from "./home/home";

export function App() {
  const [token, setToken] = useState(!!localStorage.getItem("token"));
  return (
    <div>
      <BrowserRouter>
        {token ? <Home setToken={setToken} /> : <Auth setToken={setToken} />}
      </BrowserRouter>
    </div>
  );
}
