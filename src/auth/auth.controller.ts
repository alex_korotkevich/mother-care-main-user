import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}`;

class AuthController {
  login = async (login: string, password: string) => {
    try {
      const res = await axios.post(`${baseUrl}/login`, { login, password });
      return res.data;
    } catch (err) {
      localStorage.removeItem("token");
      throw err;
    }
  };

  userRegistration = async (login: string, password: string, name: string) => {
    await axios.post(`${baseUrl}/registration`, {
      login,
      password,
      name,
    });
  };

  logout = async () => {
    try {
      const token = localStorage.getItem("token");
      await axios.get(`${baseUrl}/logout`, {
        headers: { authorization: token || "" },
      });
    } catch (err) {
      localStorage.removeItem("token");
    }
  };

  remove = async () => {
    try {
      const token = localStorage.getItem("token");
      await axios.delete(`${baseUrl}`, {
        headers: { authorization: token || "" },
      });
    } catch (err) {
      localStorage.removeItem("token");
    }
  };
}

export const authController = new AuthController();
