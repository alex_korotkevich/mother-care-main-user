import { useNavigate } from "react-router-dom";
import { authController } from "./auth.controller";

type TLogoutProps = {
  setToken: Function;
};

export const RemoveUser = (props: TLogoutProps) => {
  const navigate = useNavigate();
  const removeYourself = async () => {
    await authController.remove();
    localStorage.removeItem("token");
    props.setToken(false);
    navigate(`/registration`);
  };
  return (
    <>
      <button className="buttonRemoveUser" onClick={removeYourself}>
        <strong> REMOVE USER </strong>{" "}
      </button>
    </>
  );
};
