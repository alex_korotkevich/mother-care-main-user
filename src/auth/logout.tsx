import { useNavigate } from "react-router-dom";
import { authController } from "./auth.controller";

type TLogoutProps = {
  setToken: Function;
};

export const Logout = (props: TLogoutProps) => {
  const navigate = useNavigate();

  const logout = async () => {
    await authController.logout();
    localStorage.removeItem("token");
    props.setToken(false);
    navigate(`/`);
  };

  return (
    <>
      <button className="buttonLogout" onClick={logout}>
        <strong> LOGOUT </strong>{" "}
      </button>
    </>
  );
};
