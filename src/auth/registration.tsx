import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { ReturnButton } from "../users/buttons/return-button";
import { authController } from "./auth.controller";

export const Registration = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const navigate = useNavigate();

  const userRegistration = async () => {
    if (!login || !password || !name) {
      return;
    }
    await authController.userRegistration(login, password, name);
    navigate(`/`);
  };

  return (
    <>
      <ReturnButton />
      <div className="login_table">
        <label>
          <strong className="login_password_text">LOGIN:</strong>{" "}
          <input
            className="input_login_password"
            value={login}
            onChange={(event) => setLogin(event.target.value)}
          ></input>
        </label>
      </div>
      <div className="password_table">
        <label>
          <strong className="login_password_text"> PASSWORD:</strong>{" "}
          <input
            className="input_login_password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          ></input>
        </label>
      </div>
      <div className="name_table">
        <label>
          <strong className="login_password_text"> NAME:</strong>{" "}
          <input
            className="input_login_password"
            value={name}
            onChange={(event) => setName(event.target.value)}
          ></input>
        </label>
      </div>
      <div>
        <button className="buttonRegistration" onClick={userRegistration}>
          <span>Registration</span>
        </button>
      </div>
    </>
  );
};
