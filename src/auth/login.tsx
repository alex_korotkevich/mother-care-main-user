import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { authController } from "./auth.controller";

type TLoginProps = {
  setToken: Function;
};

export const Login = (props: TLoginProps) => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [color, setColor] = useState("");

  const navigate = useNavigate();

  const registration = async () => {
    navigate(`/registration`);
  };

  const loginButton = async () => {
    try {
      if (!login || !password) {
        return setColor("red");
      }
      setColor("");
      const token = await authController.login(login, password);
      localStorage.setItem("token", token);
      props.setToken(true);
    } catch (err) {
      setColor("red");
    }
  };

  return (
    <>
      <div className="login_table">
        <label>
          <strong className="login_password_text">LOGIN: </strong>
          <input
            className="input_login_password"
            value={login}
            onChange={(event) => setLogin(event.target.value)}
          ></input>
        </label>
      </div>

      <div className="password_table">
        <label>
          <strong className="login_password_text">PASSWORD: </strong>{" "}
          <input
            className="input_login_password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          ></input>
        </label>
      </div>
      <div>
        <button
          className="buttonLogin"
          onClick={loginButton}
          style={{ background: color }}
        >
          <strong>
            <span> LOGIN</span>
          </strong>
        </button>
      </div>
      <button className="buttonRegistration" onClick={registration}>
        <strong>
          <span> GO TO REGISTRATION</span>
        </strong>
      </button>
    </>
  );
};
