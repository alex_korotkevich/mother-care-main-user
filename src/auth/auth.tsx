import { Route, Routes } from "react-router-dom";
import { Login } from "./login";
import { Registration } from "./registration";

type TAuthProps = {
  setToken: Function;
};

export const Auth = (props: TAuthProps) => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Login setToken={props.setToken} />}></Route>
        <Route path="/registration" element={<Registration />}></Route>
      </Routes>
    </>
  );
};
