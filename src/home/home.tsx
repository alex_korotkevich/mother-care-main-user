import { useState } from "react";
import { Route, Routes } from "react-router-dom";
import { Logout } from "../auth/logout";
import { ChildCreateButton } from "../users/buttons/child-create-button";
import { NewTask } from "../users/create-task";
import { OpenChild } from "../users/open-child";
import { StartPage } from "../users/start.page";

type THomeProps = {
  setToken: Function;
};

export const Home = (props: THomeProps) => {
  const [state, setState] = useState<any[]>([]);

  const addChild = (user: any) => {
    state.push(user);
    setState([...state]);
  };
  return (
    <>
      <Logout setToken={props.setToken} />
      <Routes>
        <Route
          path="/"
          element={<StartPage setToken={props.setToken} />}
        ></Route>
        <Route
          path="/create_user"
          element={<ChildCreateButton addChild={addChild} />}
        ></Route>
        <Route path="/:id" element={<OpenChild />}></Route>
        <Route path="newtask/:id" element={<NewTask />} />
      </Routes>
    </>
  );
};
