import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}/user/tasks`;

class TaskController {
  create = async (id: number, title: string, text: string, status: string) => {
    try {
      const token = localStorage.getItem("token");
      const res = await axios.post(
        `${baseUrl}/${id}`,
        { title, text, status },
        { headers: { authorization: token || "" } }
      );
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };

  list = async (id: number) => {
    try {
      const token = localStorage.getItem("token");
      const res = await axios.get(`${baseUrl}/${id}`, {
        headers: { authorization: token || "" },
      });
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };

  remove = async (id: number) => {
    try {
      const token = localStorage.getItem("token");
      await axios.delete(`${baseUrl}/${id}`, {
        headers: { authorization: token || "" },
      });
    } catch (err) {
      console.log(err);
    }
  };
}

export const taskController = new TaskController();
