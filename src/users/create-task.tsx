import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { taskController } from "../tasks/task-controller";
import { ReturnFromCreateTaskButton } from "./buttons/return-from-create-task";

export const NewTask = () => {
  const [state, setState] = useState<any[]>([]);
  const [color, setColor] = useState("");
  const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const [status, setStatus] = useState("");
  const params: any = useParams();
  const navigate = useNavigate();

  const addNewTask = (data: any) => {
    state.push(data);
    setState([...state]);
  };

  const create = async () => {
    try {
      if (!title || !text || !status) {
        return setColor("red");
      }
      setColor("");
      const newTask = await taskController.create(
        params.id,
        title,
        text,
        status
      );
      addNewTask(newTask);
      navigate(`/${params.id}`);
    } catch (err) {
      setColor("red");
    }
  };

  return (
    <>
      <ReturnFromCreateTaskButton />
      <div>
        <div className="title_table">
          <label>
            <strong className="login_password_text"> TITLE:</strong>{" "}
            <input
              className="input_login_password"
              value={title}
              onChange={(event) => setTitle(event.target.value)}
            ></input>
          </label>
        </div>
        <div className="text_table">
          <label>
            <strong className="login_password_text">TEXT:</strong>{" "}
            <input
              className="input_login_password"
              value={text}
              onChange={(event) => setText(event.target.value)}
            ></input>
          </label>
        </div>
        <div className="status_table">
          <label>
            <strong className="login_password_text"> STATUS:</strong>
            <input
              className="input_login_password"
              value={status}
              onChange={(event) => setStatus(event.target.value)}
            ></input>
          </label>
        </div>
      </div>
      <button
        className="buttonCreate"
        style={{ background: color }}
        onClick={create}
      >
        CREATE
      </button>
    </>
  );
};
