import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { taskController } from "../tasks/task-controller";
import { ReturnButton } from "./buttons/return-button";
import { TaskRemoveButton } from "./buttons/task-remove-button";
import { childController } from "./child-controller";

type TChild = {
  id: number;
  login: string;
  password: string;
  name: string;
  userId: number;
};

export const OpenChild = () => {
  const [child, setChild] = useState<TChild>({
    id: 0,
    login: "",
    password: "",
    name: "",
    userId: 0,
  });
  const [state, setState] = useState<any[]>([]);

  const params: any = useParams();
  const navigate = useNavigate();

  const loadChildName = async () => {
    const newState = await childController.findById(params.id);
    setChild(newState);
  };

  const create = () => {
    navigate(`/newtask/${params.id}`);
  };

  const loadTask = async () => {
    const tasks = await taskController.list(params.id);
    setState(tasks);
  };

  const removeTask = (id: number) => {
    const newState = state.filter((item) => item.id !== id);
    setState(newState);
  };

  useEffect(() => {
    loadChildName();
  }, []);

  useEffect(() => {
    loadTask();
  }, []);

  if (!state.length) {
    return (
      <>
        <ReturnButton />
        <ChildItem child={child} />
        <h1 className="no_task_table">
          <strong className="no_task">This child haven't got any task</strong>
        </h1>
        <div>
          <button className="buttonCreateTask" onClick={create}>
            <span> CREATE NEW TASK</span>
          </button>
        </div>
      </>
    );
  }

  return (
    <>
      <ReturnButton />
      <ChildItem child={child} />
      <div className="all_tasks_table">
        <ol>
          {state.map((item) => (
            <TaskItem task={item} removeTask={removeTask} />
          ))}
        </ol>
      </div>
      <div>
        <button className="buttonCreateChild" onClick={create}>
          <span>CREATE NEW TASK</span>
        </button>
      </div>
    </>
  );
};
type TTaskItemProps = {
  task: {
    id: number;
    title: string;
    text: string;
    status: string;
    userId: number;
  };
  removeTask: Function;
};

const TaskItem = (props: TTaskItemProps) => {
  return (
    <>
      <div className="every_task_table">
        <ol>
          <div>
            <strong className="title_status_text">Title:</strong>{" "}
            <strong className="text_of_open_task"> {props.task.title}</strong>{" "}
          </div>

          <div>
            <strong className="title_status_text">Text: </strong>
            <strong className="text_of_open_task">{props.task.text}</strong>
          </div>

          <div>
            <strong className="title_status_text">Status:</strong>
            <strong className="text_of_open_task"> {props.task.status}</strong>
          </div>
        </ol>
        <TaskRemoveButton id={props.task.id} removeTask={props.removeTask} />
      </div>
    </>
  );
};

type TChildItemProps = {
  child: TChild;
};

const ChildItem = (props: TChildItemProps) => {
  return (
    <>
      <div>
        <strong className="you">CHILD: </strong>
        <strong className="name">{props.child.name}</strong>
      </div>
    </>
  );
};
