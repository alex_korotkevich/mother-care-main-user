import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}/childs`;

class ChildController {
  create = async (name: string, login: string, password: string) => {
    const token = localStorage.getItem("token");
    const res = await axios.post(
      `${baseUrl}`,
      {
        name,
        login,
        password,
      },
      { headers: { authorization: token || "" } }
    );
    return res.data;
  };

  findById = async (id: number) => {
    const token = localStorage.getItem("token");
    const res = await axios.get(`${baseUrl}/${id}`, {
      headers: { authorization: token || "" },
    });
    return res.data;
  };

  list = async () => {
    const token = localStorage.getItem("token");
    const res = await axios.get(`${baseUrl}`, {
      headers: { authorization: token || "" },
    });
    return res.data;
  };

  findUser = async () => {
    const token = localStorage.getItem("token");
    const res = await axios.get(`${baseUrl}/me`, {
      headers: { authorization: token || "" },
    });

    return res.data;
  };

  remove = async (id: number) => {
    const token = localStorage.getItem("token");
    await axios.delete(`${baseUrl}/${id}`, {
      headers: { authorization: token || "" },
    });
  };
}

export const childController = new ChildController();
