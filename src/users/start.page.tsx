import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { RemoveUser } from "../auth/remove.user";
import { ChildMoreButton } from "./buttons/child-more-button";
import { ChildRemoveButton } from "./buttons/child-remove-button";
import { childController } from "./child-controller";

type TUser = {
  id: number;
  login: string;
  password: string;
  name: string;
};

type THomeProps = {
  setToken: Function;
};

export const StartPage = (props: THomeProps) => {
  const [state, setState] = useState<any[]>([]);
  const [mother, setMother] = useState<TUser>({
    id: 0,
    login: "",
    password: "",
    name: "",
  });

  const navigate = useNavigate();

  const childList = async () => {
    const newState = await childController.list();
    console.log(newState);
    setState(newState);
  };

  const findUser = async () => {
    const newState = await childController.findUser();
    console.log(newState);
    setMother(newState);
  };

  useEffect(() => {
    childList();
  }, []);

  useEffect(() => {
    findUser();
  }, []);

  const removeChild = (id: number) => {
    const newState = state.filter((item) => item.id !== id);
    setState(newState);
  };

  const create = () => {
    navigate("/create_user");
  };

  if (!state.length) {
    return (
      <>
        <UserItem user={mother} />
        <h1 className="no_task_table">
          <strong className="no_task">
            You haven't got any create child yet
          </strong>
        </h1>
        <div>
          <button className="buttonCreateChild" onClick={create}>
            <strong>
              <span>CREATE NEW CHILD</span>
            </strong>
          </button>
        </div>
        <RemoveUser setToken={props.setToken} />
      </>
    );
  }

  return (
    <>
      <UserItem user={mother} />
      <div className="children_table">
        <ol>
          {state.map((item) => (
            <ChildItem child={item} removeChild={removeChild} />
          ))}
        </ol>
      </div>
      <button className="buttonCreateChild" onClick={create}>
        <strong>
          {" "}
          <span>CREATE NEW CHILD</span>
        </strong>
      </button>
      <div>
        <RemoveUser setToken={props.setToken} />
      </div>
    </>
  );
};

type TChildItem = {
  child: {
    id: number;
    login: string;
    password: string;
    name: string;
  };
  removeChild: Function;
};

const ChildItem = (props: TChildItem) => {
  return (
    <>
      <div className="every_child_table">
        <div>
          <strong className="child_name">{props.child.name}</strong>
        </div>
        <ChildRemoveButton
          id={props.child.id}
          removeChild={props.removeChild}
        />
        <ChildMoreButton id={props.child.id} />
      </div>
    </>
  );
};

type TUserItem = {
  user: TUser;
};

const UserItem = (props: TUserItem) => {
  return (
    <>
      <div>
        <strong className="you">YOU:</strong>{" "}
        <strong className="name">{props.user.name}</strong>
      </div>
    </>
  );
};
