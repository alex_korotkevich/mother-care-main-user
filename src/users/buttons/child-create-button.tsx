import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { childController } from "../child-controller";
import { ReturnButton } from "./return-button";

type TCreateProps = {
  addChild: Function;
};

export const ChildCreateButton = (props: TCreateProps) => {
  const [name, setName] = useState("");
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [color, setColor] = useState("");

  const navigate = useNavigate();

  const create = async () => {
    try {
      if (!name || !login || !password) {
        return setColor("red");
      }
      setColor("");
      const user = await childController.create(name, login, password);
      props.addChild(user);
      navigate(`/`);
    } catch (err) {
      setColor("red");
    }
  };

  return (
    <>
      <ReturnButton />
      <div className="name_table">
        <label>
          <strong className="login_password_text"> NAME: </strong>
          <input
            className="input_login_password"
            value={name}
            onChange={(event) => setName(event.target.value)}
          ></input>
        </label>
      </div>

      <div className="login_table">
        <label>
          <strong className="login_password_text"> LOGIN: </strong>
          <input
            className="input_login_password"
            value={login}
            onChange={(event) => setLogin(event.target.value)}
          ></input>
        </label>
      </div>

      <div className="password_table">
        <label>
          <strong className="login_password_text">PASSWORD: </strong>
          <input
            className="input_login_password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          ></input>
        </label>
      </div>
      <button
        style={{ background: color }}
        className="buttonCreate"
        onClick={create}
      >
        <strong>CREATE</strong>
      </button>
    </>
  );
};
