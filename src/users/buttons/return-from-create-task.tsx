import { useNavigate, useParams } from "react-router-dom";

export const ReturnFromCreateTaskButton = () => {
  const navigate = useNavigate();
  const params: any = useParams();

  const returnButton = () => {
    navigate(`/${params.id}`);
  };

  return (
    <>
      <button className="buttonReturn" onClick={returnButton}>
        <span>RETURN</span>
      </button>
    </>
  );
};
