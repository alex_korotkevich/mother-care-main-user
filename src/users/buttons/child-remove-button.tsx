import { childController } from "../child-controller";

type TRemoveProps = {
  id: number;
  removeChild: Function;
};

export const ChildRemoveButton = (props: TRemoveProps) => {
  const remove = async () => {
    await childController.remove(props.id);
    props.removeChild(props.id);
  };

  return (
    <>
      <button className="buttonRemoveChild" onClick={remove}>
        <strong>
          <span>REMOVE</span>
        </strong>
      </button>
    </>
  );
};
