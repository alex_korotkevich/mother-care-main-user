import { taskController } from "../../tasks/task-controller";

type TRemoveProps = {
  id: number;
  removeTask: Function;
};

export const TaskRemoveButton = (props: TRemoveProps) => {
  const remove = async () => {
    await taskController.remove(props.id);
    props.removeTask(props.id);
  };

  return (
    <>
      <button className="buttonRemoveChild" onClick={remove}>
        REMOVE
      </button>
    </>
  );
};
