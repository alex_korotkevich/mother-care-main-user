import { useNavigate } from "react-router-dom";

type TMoreProps = {
  id: number;
};

export const ChildMoreButton = (props: TMoreProps) => {
  const navigate = useNavigate();

  const more = () => {
    navigate(`/${props.id}`);
  };

  return (
    <>
      <button className="buttonMore" onClick={more}>
        <strong>
          <span>MORE</span>
        </strong>
      </button>
    </>
  );
};
