import { useNavigate } from "react-router-dom";

export const ReturnButton = () => {
  const navigate = useNavigate();

  const returnButton = () => {
    navigate(`/`);
  };

  return (
    <>
      <button className="buttonReturn" onClick={returnButton}>
        <span>RETURN</span>
      </button>
    </>
  );
};
